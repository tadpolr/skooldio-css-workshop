import React from 'react';
import styled from 'styled-components';

/*
 * onHover add
 * {
 *   box-shadow:
 *     rgba(32, 32, 32, 0.05) 0px 16px 16px 0px,
 *     rgba(42, 42, 42, 0.05) 0px 8px 8px 0px,
 *     rgba(49, 49, 49, 0.05) 0px 4px 4px 0px,
 *     rgba(45, 45, 45, 0.05) 0px 2px 2px 0px,
 *     rgba(35, 35, 35, 0.05) 0px 64px 64px 0px,
 *     rgba(49, 49, 49, 0.05) 0px 32px 32px 0px
 * }
 */

const Container = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  background-color: white;
  height: 100vh;
  width: 100vw;
`;

const Card = styled.div`
  background-image: url('/images/background.png');
  background-position: center;
  background-size: cover;
  width: 330px;
  height: 430px;
`;

export default function Workshop3() {
  return (
    <Container>
      <Card />
    </Container>
  );
}
