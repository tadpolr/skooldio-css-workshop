import React from 'react';

import './workshop1.css';

export default function Workshop1() {
  return (
    <div className={'container'}>
      {/* 1-3 Do not modify html */}
      <div className={'face first-face'}>
        <span className={'pip'} />
      </div>

      <div className={'face second-face'}>
        <span className={'pip'} />
        <span className={'pip'} />
      </div>

      <div className={'face third-face'}>
        <span className={'pip'} />
        <span className={'pip'} />
        <span className={'pip'} />
      </div>

      {/* 4-6 You can modify html if you want */}
      <div className={'face forth-face'}>
        <span className={'pip'} />
        <span className={'pip'} />
        <span className={'pip'} />
        <span className={'pip'} />
      </div>

      <div className={'face fifth-face'}>
        <span className={'pip'} />
        <span className={'pip'} />
        <span className={'pip'} />
        <span className={'pip'} />
        <span className={'pip'} />
      </div>

      <div className={'face sixth-face'}>
        <span className={'pip'} />
        <span className={'pip'} />
        <span className={'pip'} />
        <span className={'pip'} />
        <span className={'pip'} />
        <span className={'pip'} />
      </div>
    </div>
  );
}
