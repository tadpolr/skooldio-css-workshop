import React from 'react';
import styled from 'styled-components';
import { Link as RLink } from 'react-router-dom';

const Link = styled(RLink)`
  display: block;
`;
export default function Home() {
  return (
    <div>
      <h1>Flexbox</h1>
      <Link to={'/workshop-1'}>Workshop 1</Link>
      <Link to={'/workshop-2'}>Workshop 2</Link>
      <h1>Styled-components</h1>
      <Link to={'/workshop-3'}>Workshop 3</Link>
    </div>
  );
}
