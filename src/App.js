import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import Home from './Routes/Home';
import Workshop1 from './Routes/Workshop1';
import Workshop2 from './Routes/Workshop2';
import Workshop3 from './Routes/Workshop3';

function App() {
  return (
    <Router>
      <Route path="/" exact component={Home} />
      <Route path="/workshop-1" exact component={Workshop1} />
      <Route path="/workshop-2" component={Workshop2} />
      <Route path="/workshop-3" component={Workshop3} />
    </Router>
  );
}

export default App;
